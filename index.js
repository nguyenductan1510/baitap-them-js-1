
/**
 * Đầu vào: nhận vào ngày tháng năm mà người dùng nhập
 * Xử lý: tính ra ngày hôm qua và ngày mai từ dữ liệu đã nhận
 * Đầu ra: xuất kết quả ra màn hình cho người dùng đọc
 */

function ngayHomQua(){
    var ngayEl = document.getElementById("txt-ngay").value * 1;
    var thangEl = document.getElementById("txt-thang").value * 1;
    var namEl = document.getElementById("txt-nam").value * 1;

    var ngayHomQua;


    if(ngayEl == 1 && thangEl == 1 ){
        ngayHomQua = `${ngayEl = 31} / ${thangEl = 12} / ${namEl - 1}`;
    }
    else if(ngayEl == 1  && thangEl == 4 ){
        ngayHomQua = `${ngayEl = 31} / ${thangEl - 1} / ${namEl}`;
    }
    else if(ngayEl == 1  && thangEl == 6 ){
        ngayHomQua = `${ngayEl = 31} / ${thangEl - 1} / ${namEl}`;
    }
    else if(ngayEl == 1   && thangEl == 9 ){
        ngayHomQua = `${ngayEl = 31} / ${thangEl - 1} / ${namEl}`;
    }
    else if(ngayEl == 1   &&thangEl == 11 ){
        ngayHomQua = `${ngayEl = 31} / ${thangEl - 1} / ${namEl}`;
    }
    else if(ngayEl == 1  && thangEl == 2){
        ngayHomQua = `${ngayEl = 31} / ${thangEl - 1} / ${namEl}`;
    }
    else if(ngayEl == 1  && thangEl == 8 ){
        ngayHomQua = `${ngayEl = 31} / ${thangEl - 1} / ${namEl}`;
    }
    else if(ngayEl == 1 && thangEl == 5 ){
        ngayHomQua = `${ngayEl = 30} / ${thangEl - 1} / ${namEl}`;
    }
    else if(ngayEl == 1  && thangEl == 7 ){
        ngayHomQua = `${ngayEl = 30} / ${thangEl - 1} / ${namEl}`;
    }
    else if(ngayEl == 1  && thangEl == 10 ){
        ngayHomQua = `${ngayEl = 30} / ${thangEl - 1} / ${namEl}`;
    }
    else if(ngayEl == 1   && thangEl == 12 ){
        ngayHomQua = `${ngayEl = 30} / ${thangEl - 1} / ${namEl}`;
    }else if(ngayEl == 1   && thangEl == 3 && namEl % 4 == 0 && namEl % 100 != 0){
        ngayHomQua = `${ngayEl = 29} / ${thangEl - 1} / ${namEl}`;
    }
     else if(ngayEl == 1   && thangEl == 3 ){
        ngayHomQua = `${ngayEl = 28} / ${thangEl - 1} / ${namEl}`;
    }else if(ngayEl <= 31 && ngayEl > 1 && thangEl >= 1 && thangEl !=2 && thangEl <= 12){
        ngayHomQua = `${ngayEl -1} / ${thangEl} / ${namEl}`;
    } 
    else 
    {
        return;
    }


    document.getElementById("thongBao").innerHTML = ngayHomQua;

}

function ngayMai(){
    var ngayEl = document.getElementById("txt-ngay").value * 1;
    var thangEl = document.getElementById("txt-thang").value * 1;
    var namEl = document.getElementById("txt-nam").value * 1;

    var ngayMai;
     if(ngayEl == 31 && thangEl == 1){
        ngayMai = `${ngayEl = 1} / ${thangEl + 1} / ${namEl}`;
    }else if(ngayEl == 29 && thangEl == 2 && namEl % 4 == 0 && namEl % 100 != 0){
        ngayMai = `${ngayEl = 1} / ${thangEl + 1} / ${namEl}`;
    }else if(ngayEl == 28 && thangEl == 2){
        ngayMai = `${ngayEl = 1} / ${thangEl + 1} / ${namEl}`;
    }else if(ngayEl == 31 && thangEl == 3){
        ngayMai = `${ngayEl = 1} / ${thangEl + 1} / ${namEl}`;
    }else if(ngayEl == 30 && thangEl == 4){
        ngayMai = `${ngayEl = 1} / ${thangEl + 1} / ${namEl}`;
    }else if(ngayEl == 31 && thangEl == 5){
        ngayMai = `${ngayEl = 1} / ${thangEl + 1} / ${namEl}`;
    }else if(ngayEl == 30 && thangEl == 6){
        ngayMai = `${ngayEl = 1} / ${thangEl + 1} / ${namEl}`;
    }else if(ngayEl == 31 && thangEl == 7){
        ngayMai = `${ngayEl = 1} / ${thangEl + 1} / ${namEl}`;
    }else if(ngayEl == 31 && thangEl == 8){
        ngayMai = `${ngayEl = 1} / ${thangEl + 1} / ${namEl}`;
    }else if(ngayEl == 30 && thangEl == 9){
        ngayMai = `${ngayEl = 1} / ${thangEl + 1} / ${namEl}`;
    }else if(ngayEl == 31 && thangEl == 10){
        ngayMai = `${ngayEl = 1} / ${thangEl + 1} / ${namEl}`;
    }else if(ngayEl == 30 && thangEl == 11){
        ngayMai = `${ngayEl = 1} / ${thangEl + 1} / ${namEl}`;
    }
    else if(ngayEl == 31 && thangEl == 12){
        ngayMai = `${ngayEl = 1} / ${thangEl = 1} / ${namEl + 1}`;
    }else if(ngayEl >= 1 && ngayEl < 31 && thangEl >= 1 &&thangEl !=2 && thangEl <=12){
        ngayMai = `${ngayEl + 1} / ${thangEl } / ${namEl}`;
    }
    else{
        return;
    }


    document.getElementById("thongBao").innerHTML = ngayMai;
}

/**
 * Đầu vào: Nhận vào tháng năm mà người dùng nhập
 * Xử lý: tính số ngày của tháng đó có phải năm nhuận hay không và đưa ra số ngày
 * Đầu ra: xuất số ngày của tháng đó ra màn hình
 */

function tinhNgay(){
    var thangEl = document.getElementById("month").value*1;
    var namEl = document.getElementById("year").value*1;
    var soNgayEl;


    if(thangEl == 1  && namEl != 0){
        soNgayEl = `tháng ${thangEl} năm ${namEl} có 31 ngày`;
    }
    else if(thangEl == 2  && namEl % 4 == 0 && namEl % 100 != 0){
        soNgayEl = `tháng ${thangEl} năm ${namEl} có 29 ngày`;
    }
    else if(thangEl == 2  && namEl != 0){
        soNgayEl = `tháng ${thangEl} năm ${namEl} có 28 ngày`;
    }
    else if(thangEl == 3  && namEl != 0){
        soNgayEl = `tháng ${thangEl} năm ${namEl} có 31 ngày`;
    }
    else if(thangEl == 4  && namEl != 0){
        soNgayEl = `tháng ${thangEl} năm ${namEl} có 39 ngày`;
    }
    else if(thangEl == 5  && namEl != 0){
        soNgayEl = `tháng ${thangEl} năm ${namEl} có 31 ngày`;
    }
    else if(thangEl == 6  && namEl != 0){
        soNgayEl = `tháng ${thangEl} năm ${namEl} có 39 ngày`;
    }
    else if(thangEl == 7  && namEl != 0){
        soNgayEl = `tháng ${thangEl} năm ${namEl} có 31 ngày`;
    }
    else if(thangEl == 8  && namEl != 0){
        soNgayEl = `tháng ${thangEl} năm ${namEl} có 31 ngày`;
    }
    else if(thangEl == 9  && namEl != 0){
        soNgayEl = `tháng ${thangEl} năm ${namEl} có 39 ngày`;
    }
    else if(thangEl == 10  && namEl != 0){
        soNgayEl = `tháng ${thangEl} năm ${namEl} có 31 ngày`;
    }
    else if(thangEl == 11  && namEl != 0){
        soNgayEl = `tháng ${thangEl} năm ${namEl} có 39 ngày`;
    }
    else if(thangEl == 12  && namEl != 0){
        soNgayEl = `tháng ${thangEl} năm ${namEl} có 31 ngày`;
    }
    else
    { return };

document.getElementById("tinhngay").innerHTML = soNgayEl;
    
}

/**
 * Đầu vào: nhận vào 1 con số có 3 chữ số
 * Xử lý: đọc con số đó
 * Đầu ra: in ra cách đọc 
 */


function docSo(){
    var numberEl = document.getElementById("text-number").value * 1;
    var donVi = numberEl % 10;
    var chuc = Math.floor(numberEl / 10) % 10;
    var tram = Math.floor(numberEl / 100);
    var docSoTram;
    var docSoChuc;
    var docSoDonVi;


    if(tram == 1 && tram != 0 ){
        docSoTram = `${tram = "một trăm"} `
    }else if (tram == 2 && tram != 0){
        docSoTram = `${tram = "hai trăm"} `
    }else if (tram == 3 && tram != 0){
        docSoTram = `${tram = "ba trăm"} `
    }else if (tram == 4 && tram != 0){
        docSoTram = `${tram = "bốn trăm"} `
    }else if (tram == 5 && tram != 0){
        docSoTram = `${tram = "năm trăm"} `
    }else if (tram == 6 && tram != 0){
        docSoTram = `${tram = "sáu trăm"} `
    }else if (tram == 7 && tram != 0){
        docSoTram = `${tram = "bảy trăm"} `
    }else if (tram == 8 && tram != 0){
        docSoTram = `${tram = "tám trăm"} `
    }else if (tram == 9 && tram != 0){
        docSoTram = `${tram = "chín trăm"} `
    }else{
        return;
    }

    if(chuc == 0){
        docSoChuc = `${chuc = "lẻ"}`
    }else if(chuc == 1){
        docSoChuc = `${chuc = "mười"}`
    }else if(chuc == 2){
        docSoChuc = `${chuc = "hai mươi"}`
    }else if(chuc == 3){
        docSoChuc = `${chuc = "ba mươi"}`
    }else if(chuc == 4){
        docSoChuc = `${chuc = "bốn mươi"}`
    }else if(chuc == 5){
        docSoChuc = `${chuc = "năm mươi"}`
    }else if(chuc == 6){
        docSoChuc = `${chuc = "sáu mươi"}`
    }else if(chuc == 7){
        docSoChuc = `${chuc = "bảy mươi"}`
    }else if(chuc == 8){
        docSoChuc = `${chuc = "tám mươi"}`
    }else if(chuc == 9){
        docSoChuc = `${chuc = "chín mươi"}`
    }else{
        return
    }
    if(donVi == 0){
        docSoDonVi = `${donVi = ""}`
    }else if(donVi == 1){
        docSoDonVi = `${donVi = "một"}`
    }else if(donVi == 2){
        docSoDonVi = `${donVi = "hai"}`
    }else if(donVi == 3){
        docSoDonVi = `${donVi = "ba"}`
    }else if(donVi == 4){
        docSoDonVi = `${donVi = "bốn"}`
    }else if(donVi == 5){
        docSoDonVi = `${donVi = "năm"}`
    }else if(donVi == 6){
        docSoDonVi = `${donVi = "sáu"}`
    }else if(donVi == 7){
        docSoDonVi = `${donVi = "bảy"}`
    }else if(donVi == 8){
        docSoDonVi = `${donVi = "tám"}`
    }else if(donVi == 9){
        docSoDonVi = `${donVi = "chín"}`
    }else {
        return
    }


    document.getElementById("docso").innerHTML  = `${docSoTram} ${docSoChuc} ${docSoDonVi}`;
}



/**
 * Đầu vào: nhận vào 3 tên sinh viên và tọa độ X(Y) của 3 sinh viên đó , nhận vào tọa độ X(Y) của trường học
 * Xử lý: tính toán để tìm ra được sinh viên xa trường nhất
 * Đầu ra: Xuất kết quả ra màn hình
 */

function toaDoXy(){
    var sinhVien1 = document.getElementById("sv1").value;
    var sinhVien2 = document.getElementById("sv2").value;
    var sinhVien3 = document.getElementById("sv3").value;
    var x1 = document.getElementById("x1").value*1;
    var x2 = document.getElementById("x2").value*1;
    var x3 = document.getElementById("x3").value*1;
    var oTruong = document.getElementById("Ox").value*1;
    var y1 = document.getElementById("y1").value*1;
    var y2 = document.getElementById("y2").value*1;
    var y3 = document.getElementById("y3").value*1;
    var yTruong= document.getElementById("Oy").value*1;
    var toaDo;

    if( x1 + y1 >= oTruong + yTruong && x1 + y1 > x2 + y2 && x1 + y1 > x3 + y3){
        toaDo = `người xa trường nhất là : ${sinhVien1}`
    }else if( x1 + y1 <= oTruong + yTruong && x1 + y1 < x2 + y2 && x1 + y1 < x3 + y3){
        toaDo = `người xa trường nhất là : ${sinhVien1}`
    }
    else if(x2 + y2 >= oTruong + yTruong && x2 + y2 > x1 + y1 && x2 + y2 > x3 + y3){
        toaDo = `người xa trường nhất là : ${sinhVien2}`
    }
    else if(x2 + y2 <= oTruong + yTruong && x2 + y2 < x1 + y1 && x2 + y2 < x3 + y3){
        toaDo = `người xa trường nhất là : ${sinhVien2}`
    }else if(x3 + y3 >= oTruong + yTruong && x3 + y3 > x1 + y1 && x3 + y3 > x2 + y2 ){
        toaDo = `người xa trường nhất là : ${sinhVien3}`
    }
    else if(x3 + y3 <= oTruong + yTruong && x3 + y3 < x1 + y1 && x3 + y3 < x2 + y2 ){
        toaDo = `người xa trường nhất là : ${sinhVien3}`
    }else{return};


    document.getElementById("toado").innerHTML = toaDo;
}